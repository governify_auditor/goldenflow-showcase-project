# Goldenflow Showcase Project
Guide to simulate the golden flow and obtain metrics out of it using Governify ecosystem.

![Golden Flow Diagram](https://github.com/governifyauditor/goldenflow-showcase-project/blob/main/img/goldenflow.PNG?raw=true)

This figure shows the Golden Flow, a workflow for projects in which different teams contribute individually to a large code repository. The upstream repository is forked and each planned feature is developed by a member of the team in a separate branch. Once a feature is completed, a merge request (MR) is opened for the whole team to discuss
the changes, and if accepted, merge the changes to the forked repository's main branch. Once merged, the main branch is deployed to the staging server for the client to validate new functionality. If the client signs off on the feature, another MR is opened to merge the changes into the main repository so the feature can be included in the production server. 

## Prerequisites

### Create projects and tools
If you already have your tools setup you can skip this part.

First, you need to set up your different development tools:
 - Create Gitlab Repository
 - Create Pivotal Project
 - Create Heroku App and optionaly customize automatic deploys 
    <!-- [Guide](https://gitlab.com/governify_auditor/goldenflow-showcase-project/-/blob/main/guides/Heroku.md)  --> 

### Bluejay
 - Create info.yml file. You can use this [template](https://gitlab.com/governify_auditor/goldenflow-showcase-project/-/blob/main/info-gitlab-template.yml) and also take a look at this [example](https://gitlab.com/governify_auditor/goldenflow-showcase-project/-/blob/main/info.yml) showing what it should look like. This file will be used by Bluejay to know the different identities of your tools to evaluate and determine the different indicators.

### Governify auditor project invite
The Governify auditor should be present on the private tools in order to have access to your information:
 - GitLab: If your repository is public or it is present on your organization there is no need to add the auditor. If not, you'll need to add him by going to Project Information→Members. Enter `@governify_auditor` and accept.

![GL invite](https://gitlab.com/governify_auditor/goldenflow-showcase-project/-/raw/main/img/auditor1.png)

 - Pivotal Tracker: If your repository is public you don't need to add the auditor. If not, go to your project, click on members at the top and click on `Invite people`. Enter governify.auditor@gmail.com and add him.

![PT invite](https://gitlab.com/governify_auditor/goldenflow-showcase-project/-/raw/main/img/auditor2.PNG)

 - Heroku: You'll need to add the auditor as a collaborator. On your app dashboard view, click on `Access` at the top and then `Add collaborator`. Enter governify.auditor@gmail.com and save changes.

![H invite](https://gitlab.com/governify_auditor/goldenflow-showcase-project/-/raw/main/img/auditor3.PNG)

Bear in mind that you might need to wait for the Governify auditor to accept the invitations in order for the points to appear in the dashboard.

## Simulate golden flow
Make sure that your repository files look something like this before continuing:

![Files](https://gitlab.com/governify_auditor/goldenflow-showcase-project/-/raw/main/img/repoClear.png)

Now you can go ahead an simulate the Golden Flow.

### New branch - Started stories correlation
1. Create 4 stories on Pivotal Tracker by pressing the button `Add Story` on the top part of the column and click on the `Start` button on each to start them.
2. We are going to create 4 branches on GitHub from `main` branch. First, create one with a name of your choice.
3. Then, create 3 but this time including  in the name of the branch an ID of the four Pivotal Tracker stories created on step 1 (Check image below to see its location and the second image showing a branch name example based on the Pivotal Tracker Story ID).

`Pivotal Tracker ID location:`

![PT ID](https://gitlab.com/governify_auditor/goldenflow-showcase-project/-/raw/main/img/golden1.PNG)

`GitLab Branch name:`

![GitLab branch name](https://gitlab.com/governify_auditor/goldenflow-showcase-project/-/raw/main/img/golden2.PNG)

### Open MR - Finish story
4. Modify all the new branches adding some changes to them. 
5. Open 2 MR of the branches with the Pivotal Tracker story ID on the branch name.
6. Create another MR using the branch that doesn't have the Pivotal Tracker story ID on it.
7. Finish the 4 stories on Pivotal Tracker by clicking on the `Finish` button.

### Merge MR - Deliver story
8. First, merge one MR having the Pivotal Tracker story ID on the branch name. 
9. Now, manual deploy your app in Heroku or wait 5 minutes if you have a CD configure to ensure it is deployed before the next step.
10. After the first MR is deployed, merge the another MR but this time the one that its name does not contain the Pivotal Tracker story ID.
11. Repeat the deploy step.
12. Deliver the 4 stories by clicking on the `Deliver button`.

## Join in Bluejay's system
Follow this steps:
1. Access to [https://join.bluejay.governify.io/](https://join.bluejay.governify.io/). This is the view for joining into the system and start the tools audition.

![Join 1](https://gitlab.com/governify_auditor/goldenflow-showcase-project/-/raw/main/img/join1.PNG)

2. Enter your Repository URL. For example, `https://gitlab.com/governify_auditor/goldenflow-showcase-project` would be the URL of this Repository.
3. Click on Check. Once checked, any errors concerning your info.yml file will appear. Correct them if you have any. If no errors are found, you'll get a success message and a new section will appear.

![Join 2](https://gitlab.com/governify_auditor/goldenflow-showcase-project/-/raw/main/img/join2.PNG)

4. In the input enter a valid course code. Click on Register. If everything is ok, you will see a success and a badge will appear aswell as a markdown for adding it to your repo's README.md file. By clicking on the badge you will access to the dashboard. If you've already registered into the system you should see a message telling you so but the dashboard badge will be given so you can access the dashboard in case you lost it.

![Join 3](https://gitlab.com/governify_auditor/goldenflow-showcase-project/-/raw/main/img/join3.PNG)

The points should appear in 5 minutes or less. If you have any problem when accomplishing this section, you can contact [governify.auditor@gmail.com](mailto:governify.auditor@gmail.com) for troubleshooting.

## Check results
After doing all of this, if the data has been calculated there should appear a new point for each metric:
- At least 75% of releases must match the merge of a MR into master within ten minutes: 2/2 = 100%
- Correlation between new branches and started stories for the whole class: 3/4 = 75%
- Correlation between open merge request and finished stories for the whole class: 2/4 = 50%
- Correlation between merged merge requests and delivered stories for the whole class: 1/4 = 25%

The `Heroku releases` metric might have a different value if you didn't wait until a MR was deployed before merging a new one.

 Test.
